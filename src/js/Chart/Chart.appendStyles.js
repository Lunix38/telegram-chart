Chart.prototype.appendStyles = function() {
    this.html.main.css({
        display: 'block',
        position: 'relative',
        width: '100%',
    });

    this.html.title.css({
        display: 'block',
        position: 'relative',
        fontSize: '20px',
        fontFamily: 'PT Sans, Helvetica, Arial, sans-serif',
        color: '#486988'
    });

    this.html.chart.css({
        display: 'block',
        position: 'relative',
    });

    this.html.scrollbar.css({
        display: 'block',
        position: 'relative',
        width: '100%',
        height: '60px',
        cursor: 'pointer',
        userSelect: 'none'
    });

    this.html.scrollbarCanvas.css({
        display: 'block',
        position: 'absolute',
        top: 0,
        left: 0,
        zIndex: 1
    });

    this.html.scrollbarMask.css({
        display: 'block',
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        zIndex: 2,
        willChange: 'background-image'
    });

    this.html.scrollbarHandle.css({
        display: 'inline-block',
        position: 'relative',
        width: '100%',
        height: '100%',
        cursor: 'grab',
        borderStyle: 'solid',
        borderColor: 'rgba(179, 192, 204, 0.5)',
        borderTopWidth: '2px',
        borderBottomWidth: '2px',
        borderLeftWidth: '5px',
        borderRightWidth: '5px',
        zIndex: 3,
        willChange: 'width, left'
    });

    var sideCss = {
        display: 'block',
        position: 'absolute',
        top: '-2px',
        paddingTop: '4px',
        boxSizing: 'content-box',
        width: '5px',
        height: '100%',
        cursor: 'ew-resize'
    };

    this.html.scrollbarHandleStart.css(sideCss);
    this.html.scrollbarHandleEnd.css(sideCss);

    this.html.scrollbarHandleStart.css({
        left: '-5px'
    });

    this.html.scrollbarHandleEnd.css({
        right: '-5px'
    });
};