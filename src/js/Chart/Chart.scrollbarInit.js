Chart.prototype.scrollbarInit = function() {
    var self = this;

    this.scrollbarInitMotions();

    this.scrollbarAxes = {x: [], line: {}};

    this.scrollbarCalculateXPoints();

    for (var line in this.axesPercent.line) {
        this.scrollbarCalculateLinePoints(line);
    }

    this.drawScrollbarCanvas();

    var scrollbarResizeFn = function() {
        self.scrollbarCalculateXPoints();
        self.ctx.scrollbar.clearRect(0, 0, self.ctx.scrollbar.canvas.width, self.ctx.scrollbar.canvas.height);
        self.drawScrollbarCanvas();
    };

    window.addEvent('resize', scrollbarResizeFn);
};

Chart.prototype.scrollbarCalculateXPoints = function() {
    this.scrollbarAxes.x = [];

    for (var i = 0; i < this.axesPercent.x.length; i++) {
        var xByPixel = this.axesPercent.x[i] * this.ctx.scrollbar.canvas.clientWidth / 100;

        this.scrollbarAxes.x.push(xByPixel);
    }
};

Chart.prototype.scrollbarCalculateLinePoints = function(index) {
    this.scrollbarAxes.line[index] = [];

    var pillow = 8;

    for (var i = 0; i < this.axesPercent.line[index].length; i++) {
        var lineByPixel = this.axesPercent.line[index][i] * (this.ctx.scrollbar.canvas.clientHeight - pillow) / 100 + pillow / 2;

        this.scrollbarAxes.line[index].push(lineByPixel);
    }
};

Chart.prototype.drawScrollbarCanvas = function() {
    for (var index in this.scrollbarAxes.line) {
        this.ctx.scrollbar.beginPath();
        this.ctx.scrollbar.lineWidth = 2;
        this.ctx.scrollbar.moveTo(
            this.scrollbarAxes.x[0],
            this.scrollbarAxes.line[index][0]
        );
        this.ctx.scrollbar.strokeStyle = this.lineColors[index];

        for (var p = 1; p < this.scrollbarAxes.line[index].length; p++) {
            this.ctx.scrollbar.lineTo(
                this.scrollbarAxes.x[p],
                this.scrollbarAxes.line[index][p]
            );
        }

        this.ctx.scrollbar.stroke();
    }
};

Chart.prototype.scrollbarInitMotions = function() {
    var self = this;

    this.range = {
        start: 0,
        end: 100,
        _start: 0,
        _end: 100
    };

    var canMove = false,
        movementBlock,
        startMovePosition,
        scrollbarWidth = this.html.scrollbar.offsetWidth,
        startVal,
        endVal;

    var scrollbarMotionStart = function(e) {
        if (e.target === self.html.scrollbarHandleStart ||
            e.target === self.html.scrollbarHandleEnd ||
            e.target === self.html.scrollbarHandle
        ) {
            canMove = true;
            movementBlock = e.target;
            startMovePosition = e.clientX;

            self.range._start = self.range.start;
            self.range._end = self.range.end;

            if (e.target === self.html.scrollbarHandle) {
                document.body.style.cursor = 'grabbing';
            } else {
                document.body.style.cursor = 'ew-resize';
            }
        }
    };

    var scrollbarMotionMove = function(e) {
        if (canMove) {
            var movePos = (e.clientX - startMovePosition) / scrollbarWidth * 100;

            if (movementBlock === self.html.scrollbarHandleStart) {
                self.range.start = self.range._start + movePos;
                if (self.range.start < 0) self.range.start = 0;
                if (self.range.start + 3 >= self.range.end) self.range.start = self.range.end - 3;
            } else if (movementBlock === self.html.scrollbarHandleEnd) {
                self.range.end = self.range._end + movePos;
                if (self.range.end > 100) self.range.end = 100;
                if (self.range.end - 3 <= self.range.start) self.range.end = self.range.start + 3;
            } else if (movementBlock === self.html.scrollbarHandle) {
                self.range.start = self.range._start + movePos;
                self.range.end = self.range._end + movePos;

                if (self.range.start < 0) {
                    self.range.start = 0;
                    self.range.end = self.range._end - self.range._start;
                }

                if (self.range.end > 100) {
                    self.range.end = 100;
                    self.range.start = 100 - (self.range._end - self.range._start);
                }
            }

            if (startVal !== self.range.start || endVal !== self.range.end) {
                self.scrollbarUpdateHandle();

                // event emitter
                // console.log({
                //     start: self.range.start,
                //     end: self.range.end
                // });
            }

            startVal = self.range.start;
            endVal = self.range.end;
        }
    };

    var scrollbarMotionEnd = function() {
        if (canMove) {
            canMove = false;
            document.body.style.cursor = null;
        }
    };

    var scrollbarScrollTo = function(e) {
        if (e.target !== self.html.scrollbarHandleStart &&
            e.target !== self.html.scrollbarHandleEnd &&
            e.target !== self.html.scrollbarHandle
        ) {
            self.range._start = self.range.start;
            self.range._end = self.range.end;

            var diff = self.range._end - self.range._start;

            self.range.start = (e.offsetX / scrollbarWidth * 100) - diff / 2;

            if (self.range.start < 0) {
                self.range.start = 0;
            }

            self.range.end = self.range.start + diff;

            if (self.range.end > 100) {
                self.range.end = 100;
                self.range.start = self.range.end - diff;
            }

            self.scrollbarUpdateHandle(true);

            // event emitter
            // console.log({
            //     start: start,
            //     end: end
            // });
        }
    };

    document.addEvent('mousedown', scrollbarMotionStart);
    document.addEvent('mousemove', scrollbarMotionMove);
    document.addEvent('mouseup', scrollbarMotionEnd);
    this.html.scrollbar.addEvent('click', scrollbarScrollTo);

    var updateScrollbarwidth = function() {
        scrollbarWidth = self.html.scrollbar.offsetWidth;
    };

    window.addEvent('resize', updateScrollbarwidth);
};

Chart.prototype.scrollbarUpdateHandle = function(animate) {
    var self = this;

    if (typeof animate == "undefined") {
        animate = false;
    }

    var animationTiming = 300;
    var handleLength = this.range.end - this.range.start;

    var setHandleStyles = function (l, w) {
        self.html.scrollbarHandle.style.width = w + '%';
        self.html.scrollbarHandle.style.left = l + '%';
        self.html.scrollbarMask.style.background = 'linear-gradient(' +
            'to right,' +
            'rgba(179,192,204,0.25) 0%,' +
            'rgba(179,192,204,0.25) ' + (l) + '%,' +
            'rgba(0,0,0,0) ' + (l) + '%,' +
            'rgba(0,0,0,0) ' + (l + w) + '%,' +
            'rgba(179,192,204,0.25) ' + (l + w) + '%,' +
            'rgba(179,192,204,0.25) 100%)';
    };

    if (animate) {
        var startAnimationTime = performance.now();

        requestAnimationFrame(function animate(time) {
            var timePassed = time - startAnimationTime,
                timePerc = timePassed / animationTiming,
                updPos = (self.range.start - self.range._start) * Math.easeInOutCubic(timePerc) + self.range._start;

            if (timePassed >= animationTiming) {
                setHandleStyles(self.range.start, handleLength);

                return;
            }

            setHandleStyles(updPos, handleLength);

            requestAnimationFrame(animate);
        });
    } else {
        setHandleStyles(self.range.start, handleLength);
    }
};