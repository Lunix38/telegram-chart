Chart.prototype.buildData = function(data) {
    this.axesPercent = {x: [], line: {}};
    this.axesReal = {x: [], line: {}};
    this.lineColors = {};
    this.lineNames = {};

    this.axesData = {
        xMin: Infinity,
        xMax: 0,
        lineMin: Infinity,
        lineMax: 0
    };

    for (var c = 0; c < data.columns.length; c++) {
        var column = data.columns[c],
            columnValues = column.slice(0);

        columnValues.shift();

        var max = columnValues.max(),
            min = columnValues.min();

        if (data.types[column[0]] === 'x') {
            if (min < this.axesData.xMin) this.axesData.xMin = min;
            if (max > this.axesData.xMax) this.axesData.xMax = max;
            this.axesReal.x = columnValues;
        } else if (data.types[column[0]] === 'line') {
            if (min < this.axesData.lineMin) this.axesData.lineMin = min;
            if (max > this.axesData.lineMax) this.axesData.lineMax = max;
            this.axesReal.line[column[0]] = columnValues;
            this.lineColors[column[0]] = data.colors[column[0]];
            this.lineNames[column[0]] = data.names[column[0]];
        }
    }

    this.axesData.xRange = this.axesData.xMax - this.axesData.xMin;
    this.axesData.lineRange = this.axesData.lineMax - this.axesData.lineMin;

    this.calculateXPercent();

    for (var line in this.axesReal.line) {
        this.calculateLinePercent(line);
    }
};

Chart.prototype.calculateXPercent = function() {
    this.axesPercent.x = [];

    for (var i = 0; i < this.axesReal.x.length; i++) {
        var xInPercent = (this.axesReal.x[i] - this.axesData.xMin) / this.axesData.xRange * 100;
        this.axesPercent.x.push(xInPercent);
    }
};

Chart.prototype.calculateLinePercent = function(index) {
    this.axesPercent.line[index] = [];

    for (var i = 0; i < this.axesReal.line[index].length; i++) {
        var linePointInPercent = 100 - ((this.axesReal.line[index][i] - this.axesData.lineMin) / this.axesData.lineRange * 100);
        this.axesPercent.line[index].push(linePointInPercent);
    }
};