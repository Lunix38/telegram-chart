Chart.prototype.buildHtml = function(container) {
    var main = document.createElement('div');

    // create title
    var title = document.createElement('h3');
    title.innerText = 'I\'m a chart';
    main.appendChild(title);

    // create main canvas
    var chart = document.createElement('canvas');
    main.appendChild(chart);

    // create scrollbar
    var scrollbar = document.createElement('div');

    var scrollbarCanvas = document.createElement('canvas');
    scrollbar.appendChild(scrollbarCanvas);
    
    var scrollbarMask = document.createElement('div');
    scrollbar.appendChild(scrollbarMask);

    var scrollbarHandle = document.createElement('div');
    var scrollbarHandleStart = document.createElement('div');
    scrollbarHandle.appendChild(scrollbarHandleStart);
    var scrollbarHandleEnd = document.createElement('div');
    scrollbarHandle.appendChild(scrollbarHandleEnd);
    scrollbar.appendChild(scrollbarHandle);

    main.appendChild(scrollbar);

    this.html = {
        main: main,
        title: title,
        chart: chart,
        scrollbar: scrollbar,
        scrollbarCanvas: scrollbarCanvas,
        scrollbarMask: scrollbarMask,
        scrollbarHandle: scrollbarHandle,
        scrollbarHandleStart: scrollbarHandleStart,
        scrollbarHandleEnd: scrollbarHandleEnd
    };

    container.appendChild(main);

    this.appendStyles();
    this.initCanvas();
};