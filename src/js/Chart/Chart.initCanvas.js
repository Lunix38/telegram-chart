Chart.prototype.initCanvas = function() {
    var self = this;

    this.ctx = {};
    this.ctx.scrollbar = this.html.scrollbarCanvas.getContext('2d');
    this.ctx.chart = this.html.chart.getContext('2d');

    var setCanvasSizes = function() {
        var chartWidth = self.html.main.offsetWidth,
            chartHeight = 200;

        self.ctx.chart.setSize(chartWidth, chartHeight);

        var scrollbarWidth = self.html.scrollbar.offsetWidth,
            scrollbarHeight = self.html.scrollbar.offsetHeight;

        self.ctx.scrollbar.setSize(scrollbarWidth, scrollbarHeight);
    };

    setCanvasSizes();

    window.addEvent('resize', setCanvasSizes);
};