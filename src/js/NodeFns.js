Node.prototype.css = function(styles) {
    for (var s in styles) {
        if (typeof this.style[s] !== "undefined") { 
            this.style[s] = styles[s];
        }
    }
};

Node.prototype.addEvent = window.addEvent = function(evType, fn) {
    if (this.addEventListener) {
        this.addEventListener(evType, fn, false);
    } else if (this.attachEvent) {
        this.attachEvent('on' + evType, fn);
    } else {
        this['on' + evType] = fn;
    }
};

Array.prototype.max = function() {
    return Math.max.apply(null, this);
};

Array.prototype.min = function() {
    return Math.min.apply(null, this);
};

CanvasRenderingContext2D.prototype.setSize = function(width, height) { 
    this.canvas.width = width * window.devicePixelRatio;
    this.canvas.height = height * window.devicePixelRatio;

    this.canvas.style.width = width + 'px';
    this.canvas.style.height = height + 'px';

    this.scale(window.devicePixelRatio, window.devicePixelRatio);
};

if (typeof window.requestAnimationFrame == "undefined") {
    window.requestAnimationFrame = (function() {
        return window.webkitRequestAnimationFrame ||
               window.mozRequestAnimationFrame ||
               window.oRequestAnimationFrame ||
               window.msRequestAnimationFrame ||
               function(callback, element) {
                   window.setTimeout( callback, 1000 / 60 );
               };
    })();
}

Math.easeInOutCubic = function(t) {
    return t < 0.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1;
};