var Chart = function(container, data, options) {
    this.buildHtml(container);
    this.buildData(data);

    this.scrollbarInit();

    console.log(this);
};