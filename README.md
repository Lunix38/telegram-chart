### Install npm-dependencies

1. Install [Node.js](https://nodejs.org/)
2. Update npm `npm update -g`
3. Install dependencies `npm install`
4. Install [Gulp cli](https://gulpjs.com/)

### Project console commands

`npm run prod` — production build

`npm run dev` — developer build

`npm run watch` — watch on changes