let gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    gulpif = require('gulp-if'),
    jshint = require('gulp-jshint')

const NODE_ENV = process.env.NODE_ENV || "production"

const fldSrc = './src',
      fldDest = './build'

const jsFiles = [
    fldSrc + '/js/NodeFns.js',
    fldSrc + '/js/Chart.js',
    fldSrc + '/js/Chart/*.js',
]

let js = () => {
    return gulp.src(jsFiles)
        .pipe(plumber())
        .pipe(jshint())
        .pipe(jshint.reporter("default"))
        .pipe(gulpif(NODE_ENV === "production", uglify()))
        .pipe(concat('main.js'))
        .pipe(gulp.dest(fldDest))
}

let build = gulp.series(js)

let watch = () => {
    gulp.watch(jsFiles, js)
}

gulp.task('default', build)
gulp.task('watch', watch)