Node.prototype.css = function(styles) {
    for (var s in styles) {
        if (typeof this.style[s] !== "undefined") { 
            this.style[s] = styles[s];
        }
    }
};

Node.prototype.addEvent = window.addEvent = function(evType, fn) {
    if (this.addEventListener) {
        this.addEventListener(evType, fn, false);
    } else if (this.attachEvent) {
        this.attachEvent('on' + evType, fn);
    } else {
        this['on' + evType] = fn;
    }
};

Array.prototype.max = function() {
    return Math.max.apply(null, this);
};

Array.prototype.min = function() {
    return Math.min.apply(null, this);
};

CanvasRenderingContext2D.prototype.setSize = function(width, height) { 
    this.canvas.width = width * window.devicePixelRatio;
    this.canvas.height = height * window.devicePixelRatio;

    this.canvas.style.width = width + 'px';
    this.canvas.style.height = height + 'px';

    this.scale(window.devicePixelRatio, window.devicePixelRatio);
};

if (typeof window.requestAnimationFrame == "undefined") {
    window.requestAnimationFrame = (function() {
        return window.webkitRequestAnimationFrame ||
               window.mozRequestAnimationFrame ||
               window.oRequestAnimationFrame ||
               window.msRequestAnimationFrame ||
               function(callback, element) {
                   window.setTimeout( callback, 1000 / 60 );
               };
    })();
}

Math.easeInOutCubic = function(t) {
    return t < 0.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1;
};
var Chart = function(container, data, options) {
    this.buildHtml(container);
    this.buildData(data);

    this.scrollbarInit();

    console.log(this);
};
Chart.prototype.appendStyles = function() {
    this.html.main.css({
        display: 'block',
        position: 'relative',
        width: '100%',
    });

    this.html.title.css({
        display: 'block',
        position: 'relative',
        fontSize: '20px',
        fontFamily: 'PT Sans, Helvetica, Arial, sans-serif',
        color: '#486988'
    });

    this.html.chart.css({
        display: 'block',
        position: 'relative',
    });

    this.html.scrollbar.css({
        display: 'block',
        position: 'relative',
        width: '100%',
        height: '60px',
        cursor: 'pointer',
        userSelect: 'none'
    });

    this.html.scrollbarCanvas.css({
        display: 'block',
        position: 'absolute',
        top: 0,
        left: 0,
        zIndex: 1
    });

    this.html.scrollbarMask.css({
        display: 'block',
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        zIndex: 2,
        willChange: 'background-image'
    });

    this.html.scrollbarHandle.css({
        display: 'inline-block',
        position: 'relative',
        width: '100%',
        height: '100%',
        cursor: 'grab',
        borderStyle: 'solid',
        borderColor: 'rgba(179, 192, 204, 0.5)',
        borderTopWidth: '2px',
        borderBottomWidth: '2px',
        borderLeftWidth: '5px',
        borderRightWidth: '5px',
        zIndex: 3,
        willChange: 'width, left'
    });

    var sideCss = {
        display: 'block',
        position: 'absolute',
        top: '-2px',
        paddingTop: '4px',
        boxSizing: 'content-box',
        width: '5px',
        height: '100%',
        cursor: 'ew-resize'
    };

    this.html.scrollbarHandleStart.css(sideCss);
    this.html.scrollbarHandleEnd.css(sideCss);

    this.html.scrollbarHandleStart.css({
        left: '-5px'
    });

    this.html.scrollbarHandleEnd.css({
        right: '-5px'
    });
};
Chart.prototype.buildData = function(data) {
    this.axesPercent = {x: [], line: {}};
    this.axesReal = {x: [], line: {}};
    this.lineColors = {};
    this.lineNames = {};

    this.axesData = {
        xMin: Infinity,
        xMax: 0,
        lineMin: Infinity,
        lineMax: 0
    };

    for (var c = 0; c < data.columns.length; c++) {
        var column = data.columns[c],
            columnValues = column.slice(0);

        columnValues.shift();

        var max = columnValues.max(),
            min = columnValues.min();

        if (data.types[column[0]] === 'x') {
            if (min < this.axesData.xMin) this.axesData.xMin = min;
            if (max > this.axesData.xMax) this.axesData.xMax = max;
            this.axesReal.x = columnValues;
        } else if (data.types[column[0]] === 'line') {
            if (min < this.axesData.lineMin) this.axesData.lineMin = min;
            if (max > this.axesData.lineMax) this.axesData.lineMax = max;
            this.axesReal.line[column[0]] = columnValues;
            this.lineColors[column[0]] = data.colors[column[0]];
            this.lineNames[column[0]] = data.names[column[0]];
        }
    }

    this.axesData.xRange = this.axesData.xMax - this.axesData.xMin;
    this.axesData.lineRange = this.axesData.lineMax - this.axesData.lineMin;

    this.calculateXPercent();

    for (var line in this.axesReal.line) {
        this.calculateLinePercent(line);
    }
};

Chart.prototype.calculateXPercent = function() {
    this.axesPercent.x = [];

    for (var i = 0; i < this.axesReal.x.length; i++) {
        var xInPercent = (this.axesReal.x[i] - this.axesData.xMin) / this.axesData.xRange * 100;
        this.axesPercent.x.push(xInPercent);
    }
};

Chart.prototype.calculateLinePercent = function(index) {
    this.axesPercent.line[index] = [];

    for (var i = 0; i < this.axesReal.line[index].length; i++) {
        var linePointInPercent = 100 - ((this.axesReal.line[index][i] - this.axesData.lineMin) / this.axesData.lineRange * 100);
        this.axesPercent.line[index].push(linePointInPercent);
    }
};
Chart.prototype.buildHtml = function(container) {
    var main = document.createElement('div');

    // create title
    var title = document.createElement('h3');
    title.innerText = 'I\'m a chart';
    main.appendChild(title);

    // create main canvas
    var chart = document.createElement('canvas');
    main.appendChild(chart);

    // create scrollbar
    var scrollbar = document.createElement('div');

    var scrollbarCanvas = document.createElement('canvas');
    scrollbar.appendChild(scrollbarCanvas);
    
    var scrollbarMask = document.createElement('div');
    scrollbar.appendChild(scrollbarMask);

    var scrollbarHandle = document.createElement('div');
    var scrollbarHandleStart = document.createElement('div');
    scrollbarHandle.appendChild(scrollbarHandleStart);
    var scrollbarHandleEnd = document.createElement('div');
    scrollbarHandle.appendChild(scrollbarHandleEnd);
    scrollbar.appendChild(scrollbarHandle);

    main.appendChild(scrollbar);

    this.html = {
        main: main,
        title: title,
        chart: chart,
        scrollbar: scrollbar,
        scrollbarCanvas: scrollbarCanvas,
        scrollbarMask: scrollbarMask,
        scrollbarHandle: scrollbarHandle,
        scrollbarHandleStart: scrollbarHandleStart,
        scrollbarHandleEnd: scrollbarHandleEnd
    };

    container.appendChild(main);

    this.appendStyles();
    this.initCanvas();
};
Chart.prototype.initCanvas = function() {
    var self = this;

    this.ctx = {};
    this.ctx.scrollbar = this.html.scrollbarCanvas.getContext('2d');
    this.ctx.chart = this.html.chart.getContext('2d');

    var setCanvasSizes = function() {
        var chartWidth = self.html.main.offsetWidth,
            chartHeight = 200;

        self.ctx.chart.setSize(chartWidth, chartHeight);

        var scrollbarWidth = self.html.scrollbar.offsetWidth,
            scrollbarHeight = self.html.scrollbar.offsetHeight;

        self.ctx.scrollbar.setSize(scrollbarWidth, scrollbarHeight);
    };

    setCanvasSizes();

    window.addEvent('resize', setCanvasSizes);
};
Chart.prototype.scrollbarInit = function() {
    var self = this;

    this.scrollbarInitMotions();

    this.scrollbarAxes = {x: [], line: {}};

    this.scrollbarCalculateXPoints();

    for (var line in this.axesPercent.line) {
        this.scrollbarCalculateLinePoints(line);
    }

    this.drawScrollbarCanvas();

    var scrollbarResizeFn = function() {
        self.scrollbarCalculateXPoints();
        self.ctx.scrollbar.clearRect(0, 0, self.ctx.scrollbar.canvas.width, self.ctx.scrollbar.canvas.height);
        self.drawScrollbarCanvas();
    };

    window.addEvent('resize', scrollbarResizeFn);
};

Chart.prototype.scrollbarCalculateXPoints = function() {
    this.scrollbarAxes.x = [];

    for (var i = 0; i < this.axesPercent.x.length; i++) {
        var xByPixel = this.axesPercent.x[i] * this.ctx.scrollbar.canvas.clientWidth / 100;

        this.scrollbarAxes.x.push(xByPixel);
    }
};

Chart.prototype.scrollbarCalculateLinePoints = function(index) {
    this.scrollbarAxes.line[index] = [];

    var pillow = 8;

    for (var i = 0; i < this.axesPercent.line[index].length; i++) {
        var lineByPixel = this.axesPercent.line[index][i] * (this.ctx.scrollbar.canvas.clientHeight - pillow) / 100 + pillow / 2;

        this.scrollbarAxes.line[index].push(lineByPixel);
    }
};

Chart.prototype.drawScrollbarCanvas = function() {
    for (var index in this.scrollbarAxes.line) {
        this.ctx.scrollbar.beginPath();
        this.ctx.scrollbar.lineWidth = 2;
        this.ctx.scrollbar.moveTo(
            this.scrollbarAxes.x[0],
            this.scrollbarAxes.line[index][0]
        );
        this.ctx.scrollbar.strokeStyle = this.lineColors[index];

        for (var p = 1; p < this.scrollbarAxes.line[index].length; p++) {
            this.ctx.scrollbar.lineTo(
                this.scrollbarAxes.x[p],
                this.scrollbarAxes.line[index][p]
            );
        }

        this.ctx.scrollbar.stroke();
    }
};

Chart.prototype.scrollbarInitMotions = function() {
    var self = this;

    this.range = {
        start: 0,
        end: 100,
        _start: 0,
        _end: 100
    };

    var canMove = false,
        movementBlock,
        startMovePosition,
        scrollbarWidth = this.html.scrollbar.offsetWidth,
        startVal,
        endVal;

    var scrollbarMotionStart = function(e) {
        if (e.target === self.html.scrollbarHandleStart ||
            e.target === self.html.scrollbarHandleEnd ||
            e.target === self.html.scrollbarHandle
        ) {
            canMove = true;
            movementBlock = e.target;
            startMovePosition = e.clientX;

            self.range._start = self.range.start;
            self.range._end = self.range.end;

            if (e.target === self.html.scrollbarHandle) {
                document.body.style.cursor = 'grabbing';
            } else {
                document.body.style.cursor = 'ew-resize';
            }
        }
    };

    var scrollbarMotionMove = function(e) {
        if (canMove) {
            var movePos = (e.clientX - startMovePosition) / scrollbarWidth * 100;

            if (movementBlock === self.html.scrollbarHandleStart) {
                self.range.start = self.range._start + movePos;
                if (self.range.start < 0) self.range.start = 0;
                if (self.range.start + 3 >= self.range.end) self.range.start = self.range.end - 3;
            } else if (movementBlock === self.html.scrollbarHandleEnd) {
                self.range.end = self.range._end + movePos;
                if (self.range.end > 100) self.range.end = 100;
                if (self.range.end - 3 <= self.range.start) self.range.end = self.range.start + 3;
            } else if (movementBlock === self.html.scrollbarHandle) {
                self.range.start = self.range._start + movePos;
                self.range.end = self.range._end + movePos;

                if (self.range.start < 0) {
                    self.range.start = 0;
                    self.range.end = self.range._end - self.range._start;
                }

                if (self.range.end > 100) {
                    self.range.end = 100;
                    self.range.start = 100 - (self.range._end - self.range._start);
                }
            }

            if (startVal !== self.range.start || endVal !== self.range.end) {
                self.scrollbarUpdateHandle();

                // event emitter
                // console.log({
                //     start: self.range.start,
                //     end: self.range.end
                // });
            }

            startVal = self.range.start;
            endVal = self.range.end;
        }
    };

    var scrollbarMotionEnd = function() {
        if (canMove) {
            canMove = false;
            document.body.style.cursor = null;
        }
    };

    var scrollbarScrollTo = function(e) {
        if (e.target !== self.html.scrollbarHandleStart &&
            e.target !== self.html.scrollbarHandleEnd &&
            e.target !== self.html.scrollbarHandle
        ) {
            self.range._start = self.range.start;
            self.range._end = self.range.end;

            var diff = self.range._end - self.range._start;

            self.range.start = (e.offsetX / scrollbarWidth * 100) - diff / 2;

            if (self.range.start < 0) {
                self.range.start = 0;
            }

            self.range.end = self.range.start + diff;

            if (self.range.end > 100) {
                self.range.end = 100;
                self.range.start = self.range.end - diff;
            }

            self.scrollbarUpdateHandle(true);

            // event emitter
            // console.log({
            //     start: start,
            //     end: end
            // });
        }
    };

    document.addEvent('mousedown', scrollbarMotionStart);
    document.addEvent('mousemove', scrollbarMotionMove);
    document.addEvent('mouseup', scrollbarMotionEnd);
    this.html.scrollbar.addEvent('click', scrollbarScrollTo);

    var updateScrollbarwidth = function() {
        scrollbarWidth = self.html.scrollbar.offsetWidth;
    };

    window.addEvent('resize', updateScrollbarwidth);
};

Chart.prototype.scrollbarUpdateHandle = function(animate) {
    var self = this;

    if (typeof animate == "undefined") {
        animate = false;
    }

    var animationTiming = 300;
    var handleLength = this.range.end - this.range.start;

    var setHandleStyles = function (l, w) {
        self.html.scrollbarHandle.style.width = w + '%';
        self.html.scrollbarHandle.style.left = l + '%';
        self.html.scrollbarMask.style.background = 'linear-gradient(' +
            'to right,' +
            'rgba(179,192,204,0.25) 0%,' +
            'rgba(179,192,204,0.25) ' + (l) + '%,' +
            'rgba(0,0,0,0) ' + (l) + '%,' +
            'rgba(0,0,0,0) ' + (l + w) + '%,' +
            'rgba(179,192,204,0.25) ' + (l + w) + '%,' +
            'rgba(179,192,204,0.25) 100%)';
    };

    if (animate) {
        var startAnimationTime = performance.now();

        requestAnimationFrame(function animate(time) {
            var timePassed = time - startAnimationTime,
                timePerc = timePassed / animationTiming,
                updPos = (self.range.start - self.range._start) * Math.easeInOutCubic(timePerc) + self.range._start;

            if (timePassed >= animationTiming) {
                setHandleStyles(self.range.start, handleLength);

                return;
            }

            setHandleStyles(updPos, handleLength);

            requestAnimationFrame(animate);
        });
    } else {
        setHandleStyles(self.range.start, handleLength);
    }
};